package principal;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import objetos.Vehiculo;
import persistencia.Operador;
import utilidades.Constantes;
import utilidades.IdentificadorCampos;

public class Nucleo {
	
	private static Nucleo instancia = new Nucleo();
	
	private Nucleo() {}
	
	public static Nucleo getInstancia() {
		return instancia;
	}
	
	public void run() {
		for (int x = 8; x <= 10; x++) {
			String fichero = "recursos/export_mensual_mat_201712_" + x + ".txt";
			
			ArrayList<Vehiculo> vehiculos = IdentificadorCampos.obtenerVehiculos(fichero);
			for (int i = 0; i < vehiculos.size(); i++) {
				System.out.println("VehÝculo [" + (i + 1) + "]");
				vehiculos.get(i).recalcularCampos();
				vehiculos.get(i).mostrarCampos();
				//vehiculos.get(i).mostrarCamposOriginales();
				System.out.println();
				Operador.getFachadaPersistencia().almacenarVehiculo(vehiculos.get(i));
			}
			System.out.println("\n\n\n[" + x + "] Detectados " + vehiculos.size() + " vehÝculos.");
			try {
				FileWriter fw = new FileWriter("log.txt", true);
				fw.write(fichero + " > " + new Date().toString() + " < " + vehiculos.size() + " vehÝculos.\n");
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//for (int i = 0; i < vehiculos.size(); i++)
				//System.out.println(vehiculos.get(i));
		}
		
		
	}

}
