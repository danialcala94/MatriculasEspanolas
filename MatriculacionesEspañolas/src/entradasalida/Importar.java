package entradasalida;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Importar {
	
	private static final Importar instancia = new Importar();
	
	public Importar() {
		// Singleton
	}
	
	public Importar getInstancia() {
		return instancia;
	}
	
	/**
	 * Muestra el contenido del fichero por pantalla.
	 * @param fich
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void muestraContenido(String fich) throws FileNotFoundException, IOException {
        String cadena;
        FileReader fr = new FileReader(fich);
        BufferedReader b = new BufferedReader(fr);
        while ((cadena = b.readLine()) != null) {
            System.out.println(cadena);
        }
        b.close();
    }
	
	/**
	 * Lee el contenido �ntegro del fichero y lo devuelve sin modificaci�n alguna.
	 * @param fich
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String leerContenido(String fich) throws FileNotFoundException, IOException {
		String cadena;
		String texto = "";
	    FileReader fr = new FileReader(fich);
	    BufferedReader b = new BufferedReader(fr);
	    while ((cadena = b.readLine()) != null) {
	        texto += cadena;
	    }
	    b.close();
	    return texto;
	}
	
	public static String leerVehiculos(String fich, int numeroVehiculos) throws FileNotFoundException, IOException {
		String cadena;
		String texto = "";
		FileReader fr = new FileReader(fich);
		int caracteresPorVehiculo = 714;
		int caracteresALeer = caracteresPorVehiculo * numeroVehiculos;
		BufferedReader b = new BufferedReader(fr);
		int leido = 0;
		int vecesEscrito = 1;
		fich = fich.substring(0, fich.length() - 4);
		while ((cadena = b.readLine()) != null) {
			if (cadena.length() != 79) {
				leido += cadena.length();
				System.out.println("(" + (leido / caracteresPorVehiculo) + " / " + numeroVehiculos + ") [" + vecesEscrito + "]");
				texto += cadena;
				if (leido >= caracteresALeer) {
					BufferedWriter bw = new BufferedWriter(new FileWriter(fich + "_" + vecesEscrito + ".txt"));
					System.out.println("A escribir: " + (caracteresALeer / caracteresPorVehiculo) + " veh�culos.");
					bw.write(texto);
					texto = "";
					bw.close();
					vecesEscrito++;
					leido = 0;
				}
			}
		}
		if (leido != 0) {
			System.out.println("Hay que escribir algo m�s...");
			BufferedWriter bw = new BufferedWriter(new FileWriter(fich + "_" + vecesEscrito + ".txt"));
			System.out.println("A escribir: " + (texto.length() / caracteresPorVehiculo) + " veh�culos.");
			bw.write(texto);
			texto = "";
			bw.close();
			vecesEscrito++;
			leido = 0;
		}
		System.out.println("SE HAN ESCRITO " + vecesEscrito + " ARCHIVOS DE VEH�CULOS.");
		
		
		b.close();
		return texto;
	}
}
