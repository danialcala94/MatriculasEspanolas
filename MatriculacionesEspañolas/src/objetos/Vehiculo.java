package objetos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import utilidades.Constantes;

public class Vehiculo {
	
	public static boolean isNumeric(String cadena) {

        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }

	private String o_fechaMatriculacion;
	private String o_codigoClaseMatricula;
	private String o_fechaTramitacion;
	private String o_marcaITV;
	private String o_modeloITV;
	private String o_codigoProcedencia;
	private String o_numeroBastidor;
	private String o_codigoTipoVehiculo;
	private String o_codigoTipoPropulsion;
	private String o_cilindrada;
	private String o_potencia;
	private String o_tara;
	private String o_pesoMaximo;
	private String o_numeroPlazas;
	private String o_esPrecintado;
	private String o_esEmbargado;
	private String o_numeroTransmisiones;
	private String o_numeroTitulares;
	private String o_localidadDomicilio;
	private String o_codigoProvinciaDomicilio;
	private String o_codigoProvinciaMatricula;
	private String o_claveTramite;
	private String o_fechaTramite;
	private String o_codigoPostalDomicilio;
	private String o_fechaPrimeraMatriculacion;
	private String o_esNuevoOUsado;
	private String o_titularPersonaJuridicaOFisica;
	private String o_codigoITV;
	private String o_servicio;
	private String o_codigoINEMunicipioDomicilio;
	private String o_municipio;
	private String o_potenciaNetaMaximaKW;
	private String o_numeroMaximoPlazas;
	private String o_emisionesCO2;
	private String o_esRenting;
	private String o_codigoTutela;
	private String o_codigoPosesion;
	private String o_codigoBajaDefinitiva;
	private String o_esBajaTemporal;
	private String o_esRobado;
	private String o_esBajaTelematica;
	private String o_tipoVehiculo;
	private String o_varianteVehiculo;
	private String o_versionVehiculo;
	private String o_fabricante;
	private String o_masaEnMarcha;
	private String o_masaMaximaTecnicaAdmisible;
	private String o_categoriaHomologacionUE;
	private String o_carroceria;
	private String o_plazasPie;
	private String o_nivelEmisionesEuro;
	private String o_consumoWHKM;
	private String o_clasificacionReglamento;
	private String o_categoriaVehiculoElectrico;
	private String o_autonomiaVehiculoElectrico;
	private String o_marcaVehiculoBase;
	private String o_fabricanteVehiculoBase;
	private String o_tipoVehiculoBase;
	private String o_varianteVehiculoBase;
	private String o_versionVehiculoBase;
	private String o_distanciaEjes1y2;
	private String o_viaAnterior;
	private String o_viaPosterior;
	private String o_tipoAlimentacion;
	private String o_contrasenaHomologacion;
	private String o_ecoInnovacion;
	private String o_reduccionEco;
	private String o_codigoEco;
	private String o_fechaProceso;
	
	//	22032018
	private Date fechaMatriculacion;
	private String codigoClaseMatricula;
	private Date fechaTramitacion;
	private String marca;
	private String modelo;
	private String codigoProcedencia;
	private String numeroBastidor;
	private String codigoTipoVehiculo;
	private String codigoTipoPropulsion;
	// Puede que sea int
	private double cilindrada;
	private double potencia;
	private double tara;
	private double pesoMaximo;
	private int numeroPlazas;
	private boolean esPrecintado;
	private boolean esEmbargado;
	private int numeroTransmisiones;
	private int numeroTitulares;
	private String localidadDomicilio;
	private String provinciaDomicilio;
	private String provinciaMatriculacion;
	private String claveTramite;
	private Date fechaTramite;
	private int codigoPostalDomicilio;
	private Date fechaPrimeraMatriculacion;
	// Se podr�a llamar 'estado' (?)
	private String esNuevoOUsado;
	private String esTitularPersonaFisicaOJuridica;
	private String codigoITV;
	private String codigoServicio;
	private int codigoINEMunicipioDomicilio;
	private String municipio;
	private double potenciaNetaMaximaKW;
	private int numeroMaximoPlazas;
	private int emisionesCO2;
	private boolean esRenting;
	private String codigoTutela;
	private String codigoPosesion;
	private String codigoBajaDefinitiva;
	private boolean esBajaTemporal;
	private boolean esRobado;
	// String "En desguace" indica baja telem�tica
	private boolean esBajaTelematica;
	private String tipoVehiculo;
	private String varianteVehiculo;
	private String versionVehiculo;
	private String fabricante;
	private String masaEnMarcha;
	private String masaMaximaTecnicaAdmisible;
	private String categoriaHomologacionUE;
	private String carroceria;
	private int plazasPie;
	private String nivelEmisionesEuro;
	private String consumoWHKM;
	private String clasificacionReglamento;
	private String categoriaVehiculoElectrico;
	private String autonomiaVehiculoElectrico;
	private String marcaVehiculoBase;
	private String fabricanteVehiculoBase;
	private String tipoVehiculoBase;
	private String varianteVehiculoBase;
	private String versionVehiculoBase;
	private double distanciaEjes1y2;
	private double viaAnterior;
	private double viaPosterior;
	private String tipoAlimentacion;
	private String contrasenaHomologacion;
	private String ecoInnovacion;
	private String reduccionEco;
	private String codigoEco;
	private Date fechaProceso;
	
	
	
	public String getO_fechaMatriculacion() {
		return o_fechaMatriculacion;
	}

	public void setO_fechaMatriculacion(String o_fechaMatriculacion) {
		this.o_fechaMatriculacion = o_fechaMatriculacion;
	}

	public String getO_codigoClaseMatricula() {
		return o_codigoClaseMatricula;
	}

	public void setO_codigoClaseMatricula(String o_codigoClaseMatricula) {
		this.o_codigoClaseMatricula = o_codigoClaseMatricula;
	}

	public String getO_fechaTramitacion() {
		return o_fechaTramitacion;
	}

	public void setO_fechaTramitacion(String o_fechaTramitacion) {
		this.o_fechaTramitacion = o_fechaTramitacion;
	}

	public String getO_marcaITV() {
		return o_marcaITV;
	}

	public void setO_marcaITV(String o_marcaITV) {
		this.o_marcaITV = o_marcaITV;
	}

	public String getO_modeloITV() {
		return o_modeloITV;
	}

	public void setO_modeloITV(String o_modeloITV) {
		this.o_modeloITV = o_modeloITV;
	}

	public String getO_codigoProcedencia() {
		return o_codigoProcedencia;
	}

	public void setO_codigoProcedencia(String o_codigoProcedencia) {
		this.o_codigoProcedencia = o_codigoProcedencia;
	}

	public String getO_numeroBastidor() {
		return o_numeroBastidor;
	}

	public void setO_numeroBastidor(String o_numeroBastidor) {
		this.o_numeroBastidor = o_numeroBastidor;
	}

	public String getO_codigoTipoVehiculo() {
		return o_codigoTipoVehiculo;
	}

	public void setO_codigoTipoVehiculo(String o_codigoTipoVehiculo) {
		this.o_codigoTipoVehiculo = o_codigoTipoVehiculo;
	}

	public String getO_codigoTipoPropulsion() {
		return o_codigoTipoPropulsion;
	}

	public void setO_codigoTipoPropulsion(String o_codigoTipoPropulsion) {
		this.o_codigoTipoPropulsion = o_codigoTipoPropulsion;
	}

	public String getO_cilindrada() {
		return o_cilindrada;
	}

	public void setO_cilindrada(String o_cilindrada) {
		this.o_cilindrada = o_cilindrada;
	}

	public String getO_potencia() {
		return o_potencia;
	}

	public void setO_potencia(String o_potencia) {
		this.o_potencia = o_potencia;
	}

	public String getO_tara() {
		return o_tara;
	}

	public void setO_tara(String o_tara) {
		this.o_tara = o_tara;
	}

	public String getO_pesoMaximo() {
		return o_pesoMaximo;
	}

	public void setO_pesoMaximo(String o_pesoMaximo) {
		this.o_pesoMaximo = o_pesoMaximo;
	}

	public String getO_numeroPlazas() {
		return o_numeroPlazas;
	}

	public void setO_numeroPlazas(String o_numeroPlazas) {
		this.o_numeroPlazas = o_numeroPlazas;
	}

	public String getO_esPrecintado() {
		return o_esPrecintado;
	}

	public void setO_esPrecintado(String o_esPrecintado) {
		this.o_esPrecintado = o_esPrecintado;
	}

	public String getO_esEmbargado() {
		return o_esEmbargado;
	}

	public void setO_esEmbargado(String o_esEmbargado) {
		this.o_esEmbargado = o_esEmbargado;
	}

	public String getO_numeroTransmisiones() {
		return o_numeroTransmisiones;
	}

	public void setO_numeroTransmisiones(String o_numeroTransmisiones) {
		this.o_numeroTransmisiones = o_numeroTransmisiones;
	}

	public String getO_numeroTitulares() {
		return o_numeroTitulares;
	}

	public void setO_numeroTitulares(String o_numeroTitulares) {
		this.o_numeroTitulares = o_numeroTitulares;
	}

	public String getO_localidadDomicilio() {
		return o_localidadDomicilio;
	}

	public void setO_localidadDomicilio(String o_localidadDomicilio) {
		this.o_localidadDomicilio = o_localidadDomicilio;
	}

	public String getO_codigoProvinciaDomicilio() {
		return o_codigoProvinciaDomicilio;
	}

	public void setO_codigoProvinciaDomicilio(String o_codigoProvinciaDomicilio) {
		this.o_codigoProvinciaDomicilio = o_codigoProvinciaDomicilio;
	}

	public String getO_codigoProvinciaMatricula() {
		return o_codigoProvinciaMatricula;
	}

	public void setO_codigoProvinciaMatricula(String o_codigoProvinciaMatricula) {
		this.o_codigoProvinciaMatricula = o_codigoProvinciaMatricula;
	}

	public String getO_claveTramite() {
		return o_claveTramite;
	}

	public void setO_claveTramite(String o_claveTramite) {
		this.o_claveTramite = o_claveTramite;
	}

	public String getO_fechaTramite() {
		return o_fechaTramite;
	}

	public void setO_fechaTramite(String o_fechaTramite) {
		this.o_fechaTramite = o_fechaTramite;
	}

	public String getO_codigoPostalDomicilio() {
		return o_codigoPostalDomicilio;
	}

	public void setO_codigoPostalDomicilio(String o_codigoPostalDomicilio) {
		this.o_codigoPostalDomicilio = o_codigoPostalDomicilio;
	}

	public String getO_fechaPrimeraMatriculacion() {
		return o_fechaPrimeraMatriculacion;
	}

	public void setO_fechaPrimeraMatriculacion(String o_fechaPrimeraMatriculacion) {
		this.o_fechaPrimeraMatriculacion = o_fechaPrimeraMatriculacion;
	}

	public String getO_esNuevoOUsado() {
		return o_esNuevoOUsado;
	}

	public void setO_esNuevoOUsado(String o_esNuevoOUsado) {
		this.o_esNuevoOUsado = o_esNuevoOUsado;
	}

	public String getO_titularPersonaJuridicaOFisica() {
		return o_titularPersonaJuridicaOFisica;
	}

	public void setO_titularPersonaJuridicaOFisica(String o_titularPersonaJuridicaOFisica) {
		this.o_titularPersonaJuridicaOFisica = o_titularPersonaJuridicaOFisica;
	}

	public String getO_codigoITV() {
		return o_codigoITV;
	}

	public void setO_codigoITV(String o_codigoITV) {
		this.o_codigoITV = o_codigoITV;
	}

	public String getO_servicio() {
		return o_servicio;
	}

	public void setO_servicio(String o_servicio) {
		this.o_servicio = o_servicio;
	}

	public String getO_codigoINEMunicipioDomicilio() {
		return o_codigoINEMunicipioDomicilio;
	}

	public void setO_codigoINEMunicipioDomicilio(String o_codigoINEMunicipioDomicilio) {
		this.o_codigoINEMunicipioDomicilio = o_codigoINEMunicipioDomicilio;
	}

	public String getO_municipio() {
		return o_municipio;
	}

	public void setO_municipio(String o_municipio) {
		this.o_municipio = o_municipio;
	}

	public String getO_potenciaNetaMaximaKW() {
		return o_potenciaNetaMaximaKW;
	}

	public void setO_potenciaNetaMaximaKW(String o_potenciaNetaMaximaKW) {
		this.o_potenciaNetaMaximaKW = o_potenciaNetaMaximaKW;
	}

	public String getO_numeroMaximoPlazas() {
		return o_numeroMaximoPlazas;
	}

	public void setO_numeroMaximoPlazas(String o_numeroMaximoPlazas) {
		this.o_numeroMaximoPlazas = o_numeroMaximoPlazas;
	}

	public String getO_emisionesCO2() {
		return o_emisionesCO2;
	}

	public void setO_emisionesCO2(String o_emisionesCO2) {
		this.o_emisionesCO2 = o_emisionesCO2;
	}

	public String getO_esRenting() {
		return o_esRenting;
	}

	public void setO_esRenting(String o_esRenting) {
		this.o_esRenting = o_esRenting;
	}

	public String getO_codigoTutela() {
		return o_codigoTutela;
	}

	public void setO_codigoTutela(String o_codigoTutela) {
		this.o_codigoTutela = o_codigoTutela;
	}

	public String getO_codigoPosesion() {
		return o_codigoPosesion;
	}

	public void setO_codigoPosesion(String o_codigoPosesion) {
		this.o_codigoPosesion = o_codigoPosesion;
	}

	public String getO_codigoBajaDefinitiva() {
		return o_codigoBajaDefinitiva;
	}

	public void setO_codigoBajaDefinitiva(String o_codigoBajaDefinitiva) {
		this.o_codigoBajaDefinitiva = o_codigoBajaDefinitiva;
	}

	public String getO_esBajaTemporal() {
		return o_esBajaTemporal;
	}

	public void setO_esBajaTemporal(String o_esBajaTemporal) {
		this.o_esBajaTemporal = o_esBajaTemporal;
	}

	public String getO_esRobado() {
		return o_esRobado;
	}

	public void setO_esRobado(String o_esRobado) {
		this.o_esRobado = o_esRobado;
	}

	public String getO_esBajaTelematica() {
		return o_esBajaTelematica;
	}

	public void setO_esBajaTelematica(String o_esBajaTelematica) {
		this.o_esBajaTelematica = o_esBajaTelematica;
	}

	public String getO_tipoVehiculo() {
		return o_tipoVehiculo;
	}

	public void setO_tipoVehiculo(String o_tipoVehiculo) {
		this.o_tipoVehiculo = o_tipoVehiculo;
	}

	public String getO_varianteVehiculo() {
		return o_varianteVehiculo;
	}

	public void setO_varianteVehiculo(String o_varianteVehiculo) {
		this.o_varianteVehiculo = o_varianteVehiculo;
	}

	public String getO_versionVehiculo() {
		return o_versionVehiculo;
	}

	public void setO_versionVehiculo(String o_versionVehiculo) {
		this.o_versionVehiculo = o_versionVehiculo;
	}

	public String getO_fabricante() {
		return o_fabricante;
	}

	public void setO_fabricante(String o_fabricante) {
		this.o_fabricante = o_fabricante;
	}

	public String getO_masaEnMarcha() {
		return o_masaEnMarcha;
	}

	public void setO_masaEnMarcha(String o_masaEnMarcha) {
		this.o_masaEnMarcha = o_masaEnMarcha;
	}

	public String getO_masaMaximaTecnicaAdmisible() {
		return o_masaMaximaTecnicaAdmisible;
	}

	public void setO_masaMaximaTecnicaAdmisible(String o_masaMaximaTecnicaAdmisible) {
		this.o_masaMaximaTecnicaAdmisible = o_masaMaximaTecnicaAdmisible;
	}

	public String getO_categoriaHomologacionUE() {
		return o_categoriaHomologacionUE;
	}

	public void setO_categoriaHomologacionUE(String o_categoriaHomologacionUE) {
		this.o_categoriaHomologacionUE = o_categoriaHomologacionUE;
	}

	public String getO_carroceria() {
		return o_carroceria;
	}

	public void setO_carroceria(String o_carroceria) {
		this.o_carroceria = o_carroceria;
	}

	public String getO_plazasPie() {
		return o_plazasPie;
	}

	public void setO_plazasPie(String o_plazasPie) {
		this.o_plazasPie = o_plazasPie;
	}

	public String getO_nivelEmisionesEuro() {
		return o_nivelEmisionesEuro;
	}

	public void setO_nivelEmisionesEuro(String o_nivelEmisionesEuro) {
		this.o_nivelEmisionesEuro = o_nivelEmisionesEuro;
	}

	public String getO_consumoWHKM() {
		return o_consumoWHKM;
	}

	public void setO_consumoWHKM(String o_consumoWHKM) {
		this.o_consumoWHKM = o_consumoWHKM;
	}

	public String getO_clasificacionReglamento() {
		return o_clasificacionReglamento;
	}

	public void setO_clasificacionReglamento(String o_clasificacionReglamento) {
		this.o_clasificacionReglamento = o_clasificacionReglamento;
	}

	public String getO_categoriaVehiculoElectrico() {
		return o_categoriaVehiculoElectrico;
	}

	public void setO_categoriaVehiculoElectrico(String o_categoriaVehiculoElectrico) {
		this.o_categoriaVehiculoElectrico = o_categoriaVehiculoElectrico;
	}

	public String getO_autonomiaVehiculoElectrico() {
		return o_autonomiaVehiculoElectrico;
	}

	public void setO_autonomiaVehiculoElectrico(String o_autonomiaVehiculoElectrico) {
		this.o_autonomiaVehiculoElectrico = o_autonomiaVehiculoElectrico;
	}

	public String getO_marcaVehiculoBase() {
		return o_marcaVehiculoBase;
	}

	public void setO_marcaVehiculoBase(String o_marcaVehiculoBase) {
		this.o_marcaVehiculoBase = o_marcaVehiculoBase;
	}

	public String getO_fabricanteVehiculoBase() {
		return o_fabricanteVehiculoBase;
	}

	public void setO_fabricanteVehiculoBase(String o_fabricanteVehiculoBase) {
		this.o_fabricanteVehiculoBase = o_fabricanteVehiculoBase;
	}

	public String getO_tipoVehiculoBase() {
		return o_tipoVehiculoBase;
	}

	public void setO_tipoVehiculoBase(String o_tipoVehiculoBase) {
		this.o_tipoVehiculoBase = o_tipoVehiculoBase;
	}

	public String getO_varianteVehiculoBase() {
		return o_varianteVehiculoBase;
	}

	public void setO_varianteVehiculoBase(String o_varianteVehiculoBase) {
		this.o_varianteVehiculoBase = o_varianteVehiculoBase;
	}

	public String getO_versionVehiculoBase() {
		return o_versionVehiculoBase;
	}

	public void setO_versionVehiculoBase(String o_versionVehiculoBase) {
		this.o_versionVehiculoBase = o_versionVehiculoBase;
	}

	public String getO_distanciaEjes1y2() {
		return o_distanciaEjes1y2;
	}

	public void setO_distanciaEjes1y2(String o_distanciaEjes1y2) {
		this.o_distanciaEjes1y2 = o_distanciaEjes1y2;
	}

	public String getO_viaAnterior() {
		return o_viaAnterior;
	}

	public void setO_viaAnterior(String o_viaAnterior) {
		this.o_viaAnterior = o_viaAnterior;
	}

	public String getO_viaPosterior() {
		return o_viaPosterior;
	}

	public void setO_viaPosterior(String o_viaPosterior) {
		this.o_viaPosterior = o_viaPosterior;
	}

	public String getO_tipoAlimentacion() {
		return o_tipoAlimentacion;
	}

	public void setO_tipoAlimentacion(String o_tipoAlimentacion) {
		this.o_tipoAlimentacion = o_tipoAlimentacion;
	}

	public String getO_contrasenaHomologacion() {
		return o_contrasenaHomologacion;
	}

	public void setO_contrasenaHomologacion(String o_contrasenaHomologacion) {
		this.o_contrasenaHomologacion = o_contrasenaHomologacion;
	}

	public String getO_ecoInnovacion() {
		return o_ecoInnovacion;
	}

	public void setO_ecoInnovacion(String o_ecoInnovacion) {
		this.o_ecoInnovacion = o_ecoInnovacion;
	}

	public String getO_reduccionEco() {
		return o_reduccionEco;
	}

	public void setO_reduccionEco(String o_reduccionEco) {
		this.o_reduccionEco = o_reduccionEco;
	}

	public String getO_codigoEco() {
		return o_codigoEco;
	}

	public void setO_codigoEco(String o_codigoEco) {
		this.o_codigoEco = o_codigoEco;
	}

	public String getO_fechaProceso() {
		return o_fechaProceso;
	}

	public void setO_fechaProceso(String o_fechaProceso) {
		this.o_fechaProceso = o_fechaProceso;
	}

	public Date getFechaMatriculacion() {
		return fechaMatriculacion;
	}

	public void setFechaMatriculacion(Date fechaMatriculacion) {
		this.fechaMatriculacion = fechaMatriculacion;
	}

	public String getCodigoClaseMatricula() {
		return codigoClaseMatricula;
	}

	public void setCodigoClaseMatricula(String codigoClaseMatricula) {
		this.codigoClaseMatricula = codigoClaseMatricula;
	}

	public Date getFechaTramitacion() {
		return fechaTramitacion;
	}

	public void setFechaTramitacion(Date fechaTramitacion) {
		this.fechaTramitacion = fechaTramitacion;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getCodigoProcedencia() {
		return codigoProcedencia;
	}

	public void setCodigoProcedencia(String codigoProcedencia) {
		this.codigoProcedencia = codigoProcedencia;
	}

	public String getNumeroBastidor() {
		return numeroBastidor;
	}

	public void setNumeroBastidor(String numeroBastidor) {
		this.numeroBastidor = numeroBastidor;
	}

	public String getCodigoTipoVehiculo() {
		return codigoTipoVehiculo;
	}

	public void setCodigoTipoVehiculo(String codigoTipoVehiculo) {
		this.codigoTipoVehiculo = codigoTipoVehiculo;
	}

	public String getCodigoTipoPropulsion() {
		return codigoTipoPropulsion;
	}

	public void setCodigoTipoPropulsion(String codigoTipoPropulsion) {
		this.codigoTipoPropulsion = codigoTipoPropulsion;
	}

	public double getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(double cilindrada) {
		this.cilindrada = cilindrada;
	}

	public double getPotencia() {
		return potencia;
	}

	public void setPotencia(double potencia) {
		this.potencia = potencia;
	}

	public double getTara() {
		return tara;
	}

	public void setTara(double tara) {
		this.tara = tara;
	}

	public double getPesoMaximo() {
		return pesoMaximo;
	}

	public void setPesoMaximo(double pesoMaximo) {
		this.pesoMaximo = pesoMaximo;
	}

	public int getNumeroPlazas() {
		return numeroPlazas;
	}

	public void setNumeroPlazas(int numeroPlazas) {
		this.numeroPlazas = numeroPlazas;
	}

	public boolean isEsPrecintado() {
		return esPrecintado;
	}

	public void setEsPrecintado(boolean esPrecintado) {
		this.esPrecintado = esPrecintado;
	}

	public boolean isEsEmbargado() {
		return esEmbargado;
	}

	public void setEsEmbargado(boolean esEmbargado) {
		this.esEmbargado = esEmbargado;
	}

	public int getNumeroTransmisiones() {
		return numeroTransmisiones;
	}

	public void setNumeroTransmisiones(int numeroTransmisiones) {
		this.numeroTransmisiones = numeroTransmisiones;
	}

	public int getNumeroTitulares() {
		return numeroTitulares;
	}

	public void setNumeroTitulares(int numeroTitulares) {
		this.numeroTitulares = numeroTitulares;
	}

	public String getLocalidadDomicilio() {
		return localidadDomicilio;
	}

	public void setLocalidadDomicilio(String localidadDomicilio) {
		this.localidadDomicilio = localidadDomicilio;
	}

	public String getProvinciaDomicilio() {
		return provinciaDomicilio;
	}

	public void setProvinciaDomicilio(String provinciaDomicilio) {
		this.provinciaDomicilio = provinciaDomicilio;
	}

	public String getProvinciaMatriculacion() {
		return provinciaMatriculacion;
	}

	public void setProvinciaMatriculacion(String provinciaMatriculacion) {
		this.provinciaMatriculacion = provinciaMatriculacion;
	}

	public String getClaveTramite() {
		return claveTramite;
	}

	public void setClaveTramite(String claveTramite) {
		this.claveTramite = claveTramite;
	}

	public Date getFechaTramite() {
		return fechaTramite;
	}

	public void setFechaTramite(Date fechaTramite) {
		this.fechaTramite = fechaTramite;
	}

	public int getCodigoPostalDomicilio() {
		return codigoPostalDomicilio;
	}

	public void setCodigoPostalDomicilio(int codigoPostalDomicilio) {
		this.codigoPostalDomicilio = codigoPostalDomicilio;
	}

	public Date getFechaPrimeraMatriculacion() {
		return fechaPrimeraMatriculacion;
	}

	public void setFechaPrimeraMatriculacion(Date fechaPrimeraMatriculacion) {
		this.fechaPrimeraMatriculacion = fechaPrimeraMatriculacion;
	}

	public String getEsNuevoOUsado() {
		return esNuevoOUsado;
	}

	public void setEsNuevoOUsado(String esNuevoOUsado) {
		this.esNuevoOUsado = esNuevoOUsado;
	}

	public String getEsTitularPersonaFisicaOJuridica() {
		return esTitularPersonaFisicaOJuridica;
	}

	public void setEsTitularPersonaFisicaOJuridica(String esTitularPersonaFisicaOJuridica) {
		this.esTitularPersonaFisicaOJuridica = esTitularPersonaFisicaOJuridica;
	}

	public String getCodigoITV() {
		return codigoITV;
	}

	public void setCodigoITV(String codigoITV) {
		this.codigoITV = codigoITV;
	}

	public String getCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public int getCodigoINEMunicipioDomicilio() {
		return codigoINEMunicipioDomicilio;
	}

	public void setCodigoINEMunicipioDomicilio(int codigoINEMunicipioDomicilio) {
		this.codigoINEMunicipioDomicilio = codigoINEMunicipioDomicilio;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public double getPotenciaNetaMaximaKW() {
		return potenciaNetaMaximaKW;
	}

	public void setPotenciaNetaMaximaKW(double potenciaNetaMaximaKW) {
		this.potenciaNetaMaximaKW = potenciaNetaMaximaKW;
	}

	public int getNumeroMaximoPlazas() {
		return numeroMaximoPlazas;
	}

	public void setNumeroMaximoPlazas(int numeroMaximoPlazas) {
		this.numeroMaximoPlazas = numeroMaximoPlazas;
	}

	public int getEmisionesCO2() {
		return emisionesCO2;
	}

	public void setEmisionesCO2(int emisionesCO2) {
		this.emisionesCO2 = emisionesCO2;
	}

	public boolean isEsRenting() {
		return esRenting;
	}

	public void setEsRenting(boolean esRenting) {
		this.esRenting = esRenting;
	}

	public String getCodigoTutela() {
		return codigoTutela;
	}

	public void setCodigoTutela(String codigoTutela) {
		this.codigoTutela = codigoTutela;
	}

	public String getCodigoPosesion() {
		return codigoPosesion;
	}

	public void setCodigoPosesion(String codigoPosesion) {
		this.codigoPosesion = codigoPosesion;
	}

	public String getCodigoBajaDefinitiva() {
		return codigoBajaDefinitiva;
	}

	public void setCodigoBajaDefinitiva(String codigoBajaDefinitiva) {
		this.codigoBajaDefinitiva = codigoBajaDefinitiva;
	}

	public boolean isEsBajaTemporal() {
		return esBajaTemporal;
	}

	public void setEsBajaTemporal(boolean esBajaTemporal) {
		this.esBajaTemporal = esBajaTemporal;
	}

	public boolean isEsRobado() {
		return esRobado;
	}

	public void setEsRobado(boolean esRobado) {
		this.esRobado = esRobado;
	}

	public boolean isEsBajaTelematica() {
		return esBajaTelematica;
	}

	public void setEsBajaTelematica(boolean esBajaTelematica) {
		this.esBajaTelematica = esBajaTelematica;
	}

	public String getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public String getVarianteVehiculo() {
		return varianteVehiculo;
	}

	public void setVarianteVehiculo(String varianteVehiculo) {
		this.varianteVehiculo = varianteVehiculo;
	}

	public String getVersionVehiculo() {
		return versionVehiculo;
	}

	public void setVersionVehiculo(String versionVehiculo) {
		this.versionVehiculo = versionVehiculo;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getMasaEnMarcha() {
		return masaEnMarcha;
	}

	public void setMasaEnMarcha(String masaEnMarcha) {
		this.masaEnMarcha = masaEnMarcha;
	}

	public String getMasaMaximaTecnicaAdmisible() {
		return masaMaximaTecnicaAdmisible;
	}

	public void setMasaMaximaTecnicaAdmisible(String masaMaximaTecnicaAdmisible) {
		this.masaMaximaTecnicaAdmisible = masaMaximaTecnicaAdmisible;
	}

	public String getCategoriaHomologacionUE() {
		return categoriaHomologacionUE;
	}

	public void setCategoriaHomologacionUE(String categoriaHomologacionUE) {
		this.categoriaHomologacionUE = categoriaHomologacionUE;
	}

	public String getCarroceria() {
		return carroceria;
	}

	public void setCarroceria(String carroceria) {
		this.carroceria = carroceria;
	}

	public int getPlazasPie() {
		return plazasPie;
	}

	public void setPlazasPie(int plazasPie) {
		this.plazasPie = plazasPie;
	}

	public String getNivelEmisionesEuro() {
		return nivelEmisionesEuro;
	}

	public void setNivelEmisionesEuro(String nivelEmisionesEuro) {
		this.nivelEmisionesEuro = nivelEmisionesEuro;
	}

	public String getConsumoWHKM() {
		return consumoWHKM;
	}

	public void setConsumoWHKM(String consumoWHKM) {
		this.consumoWHKM = consumoWHKM;
	}

	public String getClasificacionReglamento() {
		return clasificacionReglamento;
	}

	public void setClasificacionReglamento(String clasificacionReglamento) {
		this.clasificacionReglamento = clasificacionReglamento;
	}

	public String getCategoriaVehiculoElectrico() {
		return categoriaVehiculoElectrico;
	}

	public void setCategoriaVehiculoElectrico(String categoriaVehiculoElectrico) {
		this.categoriaVehiculoElectrico = categoriaVehiculoElectrico;
	}

	public String getAutonomiaVehiculoElectrico() {
		return autonomiaVehiculoElectrico;
	}

	public void setAutonomiaVehiculoElectrico(String autonomiaVehiculoElectrico) {
		this.autonomiaVehiculoElectrico = autonomiaVehiculoElectrico;
	}

	public String getMarcaVehiculoBase() {
		return marcaVehiculoBase;
	}

	public void setMarcaVehiculoBase(String marcaVehiculoBase) {
		this.marcaVehiculoBase = marcaVehiculoBase;
	}

	public String getFabricanteVehiculoBase() {
		return fabricanteVehiculoBase;
	}

	public void setFabricanteVehiculoBase(String fabricanteVehiculoBase) {
		this.fabricanteVehiculoBase = fabricanteVehiculoBase;
	}

	public String getTipoVehiculoBase() {
		return tipoVehiculoBase;
	}

	public void setTipoVehiculoBase(String tipoVehiculoBase) {
		this.tipoVehiculoBase = tipoVehiculoBase;
	}

	public String getVarianteVehiculoBase() {
		return varianteVehiculoBase;
	}

	public void setVarianteVehiculoBase(String varianteVehiculoBase) {
		this.varianteVehiculoBase = varianteVehiculoBase;
	}

	public String getVersionVehiculoBase() {
		return versionVehiculoBase;
	}

	public void setVersionVehiculoBase(String versionVehiculoBase) {
		this.versionVehiculoBase = versionVehiculoBase;
	}

	public double getDistanciaEjes1y2() {
		return distanciaEjes1y2;
	}

	public void setDistanciaEjes1y2(double distanciaEjes1y2) {
		this.distanciaEjes1y2 = distanciaEjes1y2;
	}

	public double getViaAnterior() {
		return viaAnterior;
	}

	public void setViaAnterior(double viaAnterior) {
		this.viaAnterior = viaAnterior;
	}

	public double getViaPosterior() {
		return viaPosterior;
	}

	public void setViaPosterior(double viaPosterior) {
		this.viaPosterior = viaPosterior;
	}

	public String getTipoAlimentacion() {
		return tipoAlimentacion;
	}

	public void setTipoAlimentacion(String tipoAlimentacion) {
		this.tipoAlimentacion = tipoAlimentacion;
	}

	public String getContrasenaHomologacion() {
		return contrasenaHomologacion;
	}

	public void setContrasenaHomologacion(String contrasenaHomologacion) {
		this.contrasenaHomologacion = contrasenaHomologacion;
	}

	public String getEcoInnovacion() {
		return ecoInnovacion;
	}

	public void setEcoInnovacion(String ecoInnovacion) {
		this.ecoInnovacion = ecoInnovacion;
	}

	public String getReduccionEco() {
		return reduccionEco;
	}

	public void setReduccionEco(String reduccionEco) {
		this.reduccionEco = reduccionEco;
	}

	public String getCodigoEco() {
		return codigoEco;
	}

	public void setCodigoEco(String codigoEco) {
		this.codigoEco = codigoEco;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public ArrayList<String> getAtributos() {
		return atributos;
	}

	public void setAtributos(ArrayList<String> atributos) {
		this.atributos = atributos;
	}

	ArrayList<String> atributos = new ArrayList<String>();
	
	public void anadirAtributo(String atributo) {
		atributos.add(atributo);
	}
	
	public void mostrarCamposOriginales() {
		System.out.println("o_fechaMatriculacion: " + o_fechaMatriculacion);
		System.out.println("o_codigoClaseMatricula: " + o_codigoClaseMatricula);
		System.out.println("o_fechaTramitacion: " + o_fechaTramitacion);
		System.out.println("o_marcaITV: " + o_marcaITV);
		System.out.println("o_modeloITV: " + o_modeloITV);
		System.out.println("o_codigoProcedencia: " + o_codigoProcedencia);
		System.out.println("o_numeroBastidor: " + o_numeroBastidor);
		System.out.println("o_codigoTipoVehiculo: " + o_codigoTipoVehiculo);
		System.out.println("o_codigoTipoPropulsion: " + o_codigoTipoPropulsion);
		System.out.println("o_cilindrada: " + o_cilindrada);
		System.out.println("o_potencia: " + o_potencia);
		System.out.println("o_tara: " + o_tara);
		System.out.println("o_pesoMaximo: " + o_pesoMaximo);
		System.out.println("o_numeroPlazas: " + o_numeroPlazas);
		System.out.println("o_esPrecintado: " + o_esPrecintado);
		System.out.println("o_esEmbargado: " + o_esEmbargado);
		System.out.println("o_numeroTransmisiones: " + o_numeroTransmisiones);
		System.out.println("o_numeroTitulares: " + o_numeroTitulares);
		System.out.println("o_localidadDomicilio: " + o_localidadDomicilio);
		System.out.println("o_codigoProvinciaDomicilio: " + o_codigoProvinciaDomicilio);
		System.out.println("o_codigoProvinciaMatricula: " + o_codigoProvinciaMatricula);
		System.out.println("o_claveTramite: " + o_claveTramite);
		System.out.println("o_fechaTramite: " + o_fechaTramite);
		System.out.println("o_codigoPostalDomicilio: " + o_codigoPostalDomicilio);
		System.out.println("o_fechaPrimeraMatriculacion: " + o_fechaPrimeraMatriculacion);
		System.out.println("o_esNuevoOUsado: " + o_esNuevoOUsado);
		System.out.println("o_titularPersonaJuridicaOFisica: " + o_titularPersonaJuridicaOFisica);
		System.out.println("o_codigoITV: " + o_codigoITV);
		System.out.println("o_servicio: " + o_servicio);
		System.out.println("o_codigoINEMunicipioDomicilio: " + o_codigoINEMunicipioDomicilio);
		System.out.println("o_municipio: " + o_municipio);
		System.out.println("o_potenciaNetaMaximaKW: " + o_potenciaNetaMaximaKW);
		System.out.println("o_numeroMaximoPlazas: " + o_numeroMaximoPlazas);
		System.out.println("o_emisionesCO2: " + o_emisionesCO2);
		System.out.println("o_esRenting: " + o_esRenting);
		System.out.println("o_codigoTutela: " + o_codigoTutela);
		System.out.println("o_codigoPosesion: " + o_codigoPosesion);
		System.out.println("o_codigoBajaDefinitiva: " + o_codigoBajaDefinitiva);
		System.out.println("o_esBajaTemporal: " + o_esBajaTemporal);
		System.out.println("o_esRobado: " + o_esRobado);
		System.out.println("o_esBajaTelematica: " + o_esBajaTelematica);
		System.out.println("o_tipoVehiculo: " + o_tipoVehiculo);
		System.out.println("o_varianteVehiculo: " + o_varianteVehiculo);
		System.out.println("o_versionVehiculo: " + o_versionVehiculo);
		System.out.println("o_fabricante: " + o_fabricante);
		System.out.println("o_masaEnMarcha: " + o_masaEnMarcha);
		System.out.println("o_masaMaximaTecnicaAdmisible: " + o_masaMaximaTecnicaAdmisible);
		System.out.println("o_categoriaHomologacionUE: " + o_categoriaHomologacionUE);
		System.out.println("o_carroceria: " + o_carroceria);
		System.out.println("o_plazasPie: " + o_plazasPie);
		System.out.println("o_nivelEmisionesEuro: " + o_nivelEmisionesEuro);
		System.out.println("o_consumoWHKM: " + o_consumoWHKM);
		System.out.println("o_clasificacionReglamento: " + o_clasificacionReglamento);
		System.out.println("o_categoriaVehiculoElectrico: " + o_categoriaVehiculoElectrico);
		System.out.println("o_autonomiaVehiculoElectrico: " + o_autonomiaVehiculoElectrico);
		System.out.println("o_marcaVehiculoBase: " + o_marcaVehiculoBase);
		System.out.println("o_fabricanteVehiculoBase: " + o_fabricanteVehiculoBase);
		System.out.println("o_tipoVehiculoBase: " + o_tipoVehiculoBase);
		System.out.println("o_varianteVehiculoBase: " + o_varianteVehiculoBase);
		System.out.println("o_versionVehiculoBase: " + o_versionVehiculoBase);
		System.out.println("o_distanciaEjes1y2: " + o_distanciaEjes1y2);
		System.out.println("o_viaAnterior: " + o_viaAnterior);
		System.out.println("o_viaPosterior: " + o_viaPosterior);
		System.out.println("o_tipoAlimentacion: " + o_tipoAlimentacion);
		System.out.println("o_contrasenaHomologacion: " + o_contrasenaHomologacion);
		System.out.println("o_ecoInnovacion: " + o_ecoInnovacion);
		System.out.println("o_reduccionEco: " + o_reduccionEco);
		System.out.println("o_codigoEco: " + o_codigoEco);
		System.out.println("o_fechaProceso: " + o_fechaProceso);
	}
	
	public void mostrarCampos() {
		System.out.println("fechaMatriculacion: " + fechaMatriculacion);
		System.out.println("codigoClaseMatricula: " + codigoClaseMatricula);
		System.out.println("fechaTramitacion: " + fechaTramitacion);
		System.out.println("marca: " + marca);
		System.out.println("modelo: " + modelo);
		System.out.println("codigoProcedencia: " + codigoProcedencia);
		System.out.println("numeroBastidor: " + numeroBastidor);
		System.out.println("codigoTipoVehiculo: " + codigoTipoVehiculo);
		System.out.println("codigoTipoPropulsion: " + codigoTipoPropulsion);
		System.out.println("cilindrada: " + cilindrada);
		System.out.println("potencia: " + potencia);
		System.out.println("tara: " + tara);
		System.out.println("pesoMaximo: " + pesoMaximo);
		System.out.println("numeroPlazas: " + numeroPlazas);
		System.out.println("esPrecintado: " + esPrecintado);
		System.out.println("esEmbargado: " + esEmbargado);
		System.out.println("numeroTransmisiones: " + numeroTransmisiones);
		System.out.println("numeroTitulares: " + numeroTitulares);
		System.out.println("localidadDomicilio: " + localidadDomicilio);
		System.out.println("provinciaDomicilio: " + provinciaDomicilio);
		System.out.println("provinciaMatriculacion: " + provinciaMatriculacion);
		System.out.println("claveTramite: " + claveTramite);
		System.out.println("fechaTramite: " + fechaTramite);
		System.out.println("codigoPostalDomicilio: " + codigoPostalDomicilio);
		System.out.println("fechaPrimeraMatriculacion: " + fechaPrimeraMatriculacion);
		System.out.println("esNuevoOUsado: " + esNuevoOUsado);
		System.out.println("esTitularPersonaJuridicaOFisica: " + esTitularPersonaFisicaOJuridica);
		System.out.println("codigoITV: " + codigoITV);
		System.out.println("codigoServicio: " + codigoServicio);
		System.out.println("codigoINEMunicipioDomicilio: " + codigoINEMunicipioDomicilio);
		System.out.println("municipio: " + municipio);
		System.out.println("potenciaNetaMaximaKW: " + potenciaNetaMaximaKW);
		System.out.println("numeroMaximoPlazas: " + numeroMaximoPlazas);
		System.out.println("emisionesCO2: " + emisionesCO2);
		System.out.println("esRenting: " + esRenting);
		System.out.println("codigoTutela: " + codigoTutela);
		System.out.println("codigoPosesion: " + codigoPosesion);
		System.out.println("codigoBajaDefinitiva: " + codigoBajaDefinitiva);
		System.out.println("esBajaTemporal: " + esBajaTemporal);
		System.out.println("esRobado: " + esRobado);
		System.out.println("esBajaTelematica: " + esBajaTelematica);
		System.out.println("tipoVehiculo: " + tipoVehiculo);
		System.out.println("varianteVehiculo: " + varianteVehiculo);
		System.out.println("versionVehiculo: " + versionVehiculo);
		System.out.println("fabricante: " + fabricante);
		System.out.println("masaEnMarcha: " + masaEnMarcha);
		System.out.println("masaMaximaTecnicaAdmisible: " + masaMaximaTecnicaAdmisible);
		System.out.println("categoriaHomologacionUE: " + categoriaHomologacionUE);
		System.out.println("carroceria: " + carroceria);
		System.out.println("plazasPie: " + plazasPie);
		System.out.println("nivelEmisionesEuro: " + nivelEmisionesEuro);
		System.out.println("consumoWHKM: " + consumoWHKM);
		System.out.println("clasificacionReglamento: " + clasificacionReglamento);
		System.out.println("categoriaVehiculoElectrico: " + categoriaVehiculoElectrico);
		System.out.println("autonomiaVehiculoElectrico: " + autonomiaVehiculoElectrico);
		System.out.println("marcaVehiculoBase: " + marcaVehiculoBase);
		System.out.println("fabricanteVehiculoBase: " + fabricanteVehiculoBase);
		System.out.println("tipoVehiculoBase: " + tipoVehiculoBase);
		System.out.println("varianteVehiculoBase: " + varianteVehiculoBase);
		System.out.println("versionVehiculoBase: " + versionVehiculoBase);
		System.out.println("distanciaEjes1y2: " + distanciaEjes1y2);
		System.out.println("viaAnterior: " + viaAnterior);
		System.out.println("viaPosterior: " + viaPosterior);
		System.out.println("tipoAlimentacion: " + tipoAlimentacion);
		System.out.println("contrasenaHomologacion: " + contrasenaHomologacion);
		System.out.println("ecoInnovacion: " + ecoInnovacion);
		System.out.println("reduccionEco: " + reduccionEco);
		System.out.println("codigoEco: " + codigoEco);
		System.out.println("fechaProceso: " + fechaProceso);
	}
	
	public void recalcularCampos() {
		// fechaMatriculaci�n
		try {
			setFechaMatriculacion(new SimpleDateFormat("ddMMyyyy").parse(o_fechaMatriculacion));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("[ERROR] No se ha podido calcular el valor fechaMatriculacion.");
			e.printStackTrace();
		}
		// codigoClaseMatricula
		switch (o_codigoClaseMatricula) {
			case "0":
				setCodigoClaseMatricula(Constantes.COD_CLASEMATRICULA_0);
				break;
			case "1":
				setCodigoClaseMatricula(Constantes.COD_CLASEMATRICULA_1);
				break;
			case "2":
				setCodigoClaseMatricula(Constantes.COD_CLASEMATRICULA_2);
				break;
			case "3":
				setCodigoClaseMatricula(Constantes.COD_CLASEMATRICULA_3);
				break;
			case "4":
				setCodigoClaseMatricula(Constantes.COD_CLASEMATRICULA_4);
				break;
			case "5":
				setCodigoClaseMatricula(Constantes.COD_CLASEMATRICULA_5);
				break;
			case "6":
				setCodigoClaseMatricula(Constantes.COD_CLASEMATRICULA_6);
				break;
			case "7":
				setCodigoClaseMatricula(Constantes.COD_CLASEMATRICULA_7);
				break;
			case "8":
				setCodigoClaseMatricula(Constantes.COD_CLASEMATRICULA_8);
				break;
		}
		// fechaTramitacion
		if (!o_fechaTramitacion.isEmpty()) {
			try {
				setFechaTramitacion(new SimpleDateFormat("ddMMyyyy").parse(o_fechaTramitacion));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("[ERROR] No se ha podido calcular el valor fechaTramitacion.");
				e.printStackTrace();
			}
		}
		// marca
		setMarca(o_marcaITV);
		// modelo
		setModelo(o_modeloITV);
		// codigoProcedencia
		switch (o_codigoProcedencia) {
			case "0":
				setCodigoProcedencia(Constantes.COD_PROCEDENCIA_0);
				break;
			case "1":
				setCodigoProcedencia(Constantes.COD_PROCEDENCIA_1);
				break;
			case "2":
				setCodigoProcedencia(Constantes.COD_PROCEDENCIA_2);
				break;
			case "3":
				setCodigoProcedencia(Constantes.COD_PROCEDENCIA_3);
				break;
		}
		// numeroBastidor
		setNumeroBastidor(o_numeroBastidor);
		// codigoTipoVehiculo
		setCodigoTipoVehiculo(o_codigoTipoVehiculo);
		// codigoTipoPropulsion
		setCodigoTipoPropulsion(o_codigoTipoPropulsion);
		// cilindrada
		if (o_cilindrada.isEmpty() || !isNumeric(o_cilindrada))
			setCilindrada(Constantes.VALOR_INDEFINIDO);
		else
			setCilindrada(Double.parseDouble(o_cilindrada));
		// potencia
		if (o_potencia.isEmpty() || !isNumeric(o_potencia))
			setPotencia(Constantes.VALOR_INDEFINIDO);
		else
			setPotencia(Double.parseDouble(o_potencia));
		// tara
		if (o_tara.isEmpty() || !isNumeric(o_tara))
			setTara(Constantes.VALOR_INDEFINIDO);
		else
			setTara(Double.parseDouble(o_tara));
		// pesoMaximo
		if (o_pesoMaximo.isEmpty() || !isNumeric(o_pesoMaximo))
			setPesoMaximo(Constantes.VALOR_INDEFINIDO);
		else
			setPesoMaximo(Double.parseDouble(o_pesoMaximo));
		// numeroPlazas
		if (o_numeroPlazas.isEmpty() || !isNumeric(o_numeroPlazas))
			setNumeroPlazas(Constantes.VALOR_INDEFINIDO);
		else
			setNumeroPlazas(Integer.parseInt(o_numeroPlazas));
		// esPrecintado
		if (o_esPrecintado.equalsIgnoreCase("si"))
			setEsPrecintado(true);
		else
			setEsPrecintado(false);
		// esEmbargado
		if (o_esEmbargado.equalsIgnoreCase("si"))
			setEsEmbargado(true);
		else
			setEsEmbargado(false);
		// numeroTransmisiones
		if (o_numeroTransmisiones.isEmpty() || !isNumeric(o_numeroTransmisiones))
			setNumeroTransmisiones(Constantes.VALOR_INDEFINIDO);
		else
			setNumeroTransmisiones(Integer.parseInt(o_numeroTransmisiones));
		// numeroTitulares
		if (o_numeroTitulares.isEmpty() || !isNumeric(o_numeroTitulares))
			setNumeroTitulares(Constantes.VALOR_INDEFINIDO);
		else
			setNumeroTitulares(Integer.parseInt(o_numeroTitulares));
		// localidadDomicilio
		setLocalidadDomicilio(o_localidadDomicilio);
		// provinciaDomicilio
		switch(o_codigoProvinciaDomicilio) {
		case "A":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_A);
			break;
		case "AB":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_AB);
			break;
		case "AL":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_AL);
			break;
		case "AV":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_AV);
			break;
		case "B":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_B);
			break;
		case "BA":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_BA);
			break;
		case "BI":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_BI);
			break;
		case "BU":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_BU);
			break;
		case "C":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_C);
			break;
		case "CA":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_CA);
			break;
		case "CC":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_CC);
			break;
		case "CE":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_CE);
			break;
		case "CO":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_CO);
			break;
		case "CR":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_CR);
			break;
		case "CS":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_CS);
			break;
		case "CU":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_CU);
			break;
		case "DS":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_DS);
			break;
		case "EX":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_EX);
			break;
		case "GC":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_GC);
			break;
		case "GI":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_GI);
			break;
		case "GR":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_GR);
			break;
		case "GU":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_GU);
			break;
		case "H":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_H);
			break;
		case "HU":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_HU);
			break;
		case "J":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_J);
			break;
		case "L":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_L);
			break;
		case "LE":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_LE);
			break;
		case "LO":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_LO);
			break;
		case "LU":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_LU);
			break;
		case "M":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_M);
			break;
		case "MA":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_MA);
			break;
		case "ML":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_ML);
			break;
		case "MU":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_MU);
			break;
		case "NA":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_NA);
			break;
		case "O":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_O);
			break;
		case "OU":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_OU);
			break;
		case "P":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_P);
			break;
		case "IB":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_IB);
			break;
		case "PO":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_PO);
			break;
		case "S":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_S);
			break;
		case "SA":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_SA);
			break;
		case "SE":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_SE);
			break;
		case "SG":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_SG);
			break;
		case "SO":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_SO);
			break;
		case "SS":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_S);
			break;
		case "T":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_T);
			break;
		case "TE":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_TE);
			break;
		case "TF":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_TF);
			break;
		case "TO":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_TO);
			break;
		case "V":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_V);
			break;
		case "VA":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_VA);
			break;
		case "VI":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_VI);
			break;
		case "Z":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_Z);
			break;
		case "ZA":
			setProvinciaDomicilio(Constantes.COD_PROVINCIA_ZA);
			break;
		}
		// provinciaMatriculacion
		switch(o_codigoProvinciaMatricula) {
		case "A":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_A);
			break;
		case "AB":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_AB);
			break;
		case "AL":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_AL);
			break;
		case "AV":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_AV);
			break;
		case "B":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_B);
			break;
		case "BA":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_BA);
			break;
		case "BI":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_BI);
			break;
		case "BU":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_BU);
			break;
		case "C":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_C);
			break;
		case "CA":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_CA);
			break;
		case "CC":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_CC);
			break;
		case "CE":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_CE);
			break;
		case "CO":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_CO);
			break;
		case "CR":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_CR);
			break;
		case "CS":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_CS);
			break;
		case "CU":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_CU);
			break;
		case "DS":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_DS);
			break;
		case "EX":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_EX);
			break;
		case "GC":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_GC);
			break;
		case "GI":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_GI);
			break;
		case "GR":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_GR);
			break;
		case "GU":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_GU);
			break;
		case "H":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_H);
			break;
		case "HU":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_HU);
			break;
		case "J":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_J);
			break;
		case "L":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_L);
			break;
		case "LE":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_LE);
			break;
		case "LO":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_LO);
			break;
		case "LU":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_LU);
			break;
		case "M":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_M);
			break;
		case "MA":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_MA);
			break;
		case "ML":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_ML);
			break;
		case "MU":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_MU);
			break;
		case "NA":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_NA);
			break;
		case "O":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_O);
			break;
		case "OU":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_OU);
			break;
		case "P":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_P);
			break;
		case "IB":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_IB);
			break;
		case "PO":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_PO);
			break;
		case "S":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_S);
			break;
		case "SA":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_SA);
			break;
		case "SC":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_SC);
			break;
		case "SE":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_SE);
			break;
		case "SG":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_SG);
			break;
		case "SO":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_SO);
			break;
		case "SS":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_S);
			break;
		case "T":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_T);
			break;
		case "TE":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_TE);
			break;
		case "TF":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_TF);
			break;
		case "TO":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_TO);
			break;
		case "V":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_V);
			break;
		case "VA":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_VA);
			break;
		case "VI":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_VI);
			break;
		case "Z":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_Z);
			break;
		case "ZA":
			setProvinciaMatriculacion(Constantes.COD_PROVINCIA_ZA);
			break;
		}
		// codigoTramite
		switch (o_claveTramite) {
		case "1":
			setClaveTramite(Constantes.COD_TRAMITE_1);
			break;
		case "2":
			setClaveTramite(Constantes.COD_TRAMITE_2);
			break;
		case "3":
			setClaveTramite(Constantes.COD_TRAMITE_3);
			break;
		case "4":
			setClaveTramite(Constantes.COD_TRAMITE_4);
			break;
		case "5":
			setClaveTramite(Constantes.COD_TRAMITE_5);
			break;
		case "6":
			setClaveTramite(Constantes.COD_TRAMITE_6);
			break;
		case "7":
			setClaveTramite(Constantes.COD_TRAMITE_7);
			break;
		case "8":
			setClaveTramite(Constantes.COD_TRAMITE_8);
			break;
		case "9":
			setClaveTramite(Constantes.COD_TRAMITE_9);
			break;
		case "A":
			setClaveTramite(Constantes.COD_TRAMITE_A);
			break;
		case "B":
			setClaveTramite(Constantes.COD_TRAMITE_B);
			break;
		}
		// fechaTramite
		try {
			setFechaTramite(new SimpleDateFormat("ddMMyyyy").parse(o_fechaTramite));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("[ERROR] No se ha podido calcular el valor fechaTramite.");
			e.printStackTrace();
		}
		// codigoPostalDomicilio
		if (o_codigoPostalDomicilio.isEmpty() || !isNumeric(o_codigoPostalDomicilio))
			setCodigoPostalDomicilio(Constantes.VALOR_INDEFINIDO);
		else
			setCodigoPostalDomicilio(Integer.parseInt(o_codigoPostalDomicilio));
		// fechaPrimeraMatriculacion
		if (!o_fechaPrimeraMatriculacion.isEmpty()) {
			try {
				setFechaPrimeraMatriculacion(new SimpleDateFormat("ddMMyyyy").parse(o_fechaPrimeraMatriculacion));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("[ERROR] No se ha podido calcular el valor fechaPrimeraMatriculacion.");
				e.printStackTrace();
			}
		} else
			setFechaPrimeraMatriculacion(null);
		// esNuevoOUsado
		if (o_esNuevoOUsado.isEmpty())
			setEsNuevoOUsado(Constantes.NO_HAY_DATOS_REGISTRADOS);
		else {
			if (o_esNuevoOUsado.equalsIgnoreCase("n"))
				setEsNuevoOUsado(Constantes.VEHICULO_NUEVO);
			else if (o_esNuevoOUsado.equalsIgnoreCase("u"))
				setEsNuevoOUsado(Constantes.VEHICULO_USADO);
		}
		// esTitularPersonaJuridicaOFisica
		if (o_titularPersonaJuridicaOFisica.isEmpty())
			setEsTitularPersonaFisicaOJuridica(Constantes.NO_HAY_DATOS_REGISTRADOS);
		else {
			if (o_titularPersonaJuridicaOFisica.equalsIgnoreCase("d"))
				setEsTitularPersonaFisicaOJuridica(Constantes.PERSONA_FISICA);
			else if (o_titularPersonaJuridicaOFisica.equalsIgnoreCase("x"))
				setEsTitularPersonaFisicaOJuridica(Constantes.PERSONA_JURIDICA);
		}
		// codigoITV
		setCodigoITV(o_codigoITV);
		// codigoServicio
		switch (o_servicio) {
		case "0":
			setCodigoServicio(Constantes.COD_SERVICIO_0);
			break;
		case "1":
			setCodigoServicio(Constantes.COD_SERVICIO_1);
			break;
		case "2":
			setCodigoServicio(Constantes.COD_SERVICIO_2);
			break;
		case "3":
			setCodigoServicio(Constantes.COD_SERVICIO_3);
			break;
		case "4":
			setCodigoServicio(Constantes.COD_SERVICIO_4);
			break;
		case "5":
			setCodigoServicio(Constantes.COD_SERVICIO_5);
			break;
		case "6":
			setCodigoServicio(Constantes.COD_SERVICIO_6);
			break;
		case "7":
			setCodigoServicio(Constantes.COD_SERVICIO_7);
			break;
		case "8":
			setCodigoServicio(Constantes.COD_SERVICIO_8);
			break;
		case "9":
			setCodigoServicio(Constantes.COD_SERVICIO_9);
			break;
		}
		// codigoINEMunicipioDomicilio
		if (o_codigoINEMunicipioDomicilio.isEmpty() || !isNumeric(o_codigoINEMunicipioDomicilio))
			setCodigoINEMunicipioDomicilio(Constantes.VALOR_INDEFINIDO);
		else
			setCodigoINEMunicipioDomicilio(Integer.parseInt(o_codigoINEMunicipioDomicilio));
		// municipio
		setMunicipio(o_municipio);
		// potenciaNetaMaximaKW
		if (o_potenciaNetaMaximaKW.isEmpty() || !isNumeric(o_potenciaNetaMaximaKW))
			setPotenciaNetaMaximaKW(Constantes.VALOR_INDEFINIDO);
		else
			setPotenciaNetaMaximaKW(Double.parseDouble(o_potenciaNetaMaximaKW));
		// numeroMaximoPlazas
		if (o_numeroMaximoPlazas.isEmpty() || !isNumeric(o_numeroMaximoPlazas))
			setNumeroMaximoPlazas(Constantes.VALOR_INDEFINIDO);
		else
			setNumeroMaximoPlazas(Integer.parseInt(o_numeroMaximoPlazas));
		// emisionesCO2
		if (o_emisionesCO2.isEmpty() || !isNumeric(o_emisionesCO2))
			setEmisionesCO2(Constantes.VALOR_INDEFINIDO);
		else
			setEmisionesCO2(Integer.parseInt(o_emisionesCO2));
		// esRenting
		if (o_esRenting.isEmpty())
			setEsRenting(false);
		else {
			if (o_esRenting.equalsIgnoreCase("s"))
				setEsRenting(true);
			else if (o_esRenting.equalsIgnoreCase("n"))
				setEsRenting(false);
		}
		// codigoTutela
		setCodigoTutela(o_codigoTutela);
		// codigoPosesion
		setCodigoPosesion(o_codigoPosesion);
		// codigoBajaDefinitiva
		setCodigoBajaDefinitiva(o_codigoBajaDefinitiva);
		// esBajaTemporal
		if (o_esBajaTemporal.isEmpty())
			setEsBajaTemporal(false);
		else {
			if (o_esBajaTemporal.equalsIgnoreCase("s"))
				setEsBajaTemporal(true);
			else if (o_esBajaTemporal.equalsIgnoreCase("n"))
				setEsBajaTemporal(false);
		}
		// esRobado
		if (o_esRobado.isEmpty())
			setEsRobado(false);
		else {
			if (o_esRobado.equalsIgnoreCase("s"))
				setEsRobado(true);
			else if (o_esRobado.equalsIgnoreCase("n"))
				setEsRobado(false);
		}
		// esBajaTelematica
		if (o_esBajaTelematica.isEmpty())
			setEsBajaTelematica(false);
		else if (o_esBajaTelematica.equals(Constantes.BAJA_TELEMATICA))
			setEsBajaTelematica(true);
		// tipoVehiculo (LO HE AUTOGENERADO A PARTIR DEL codigoTipoVehiculo)
		switch (o_codigoTipoVehiculo) {
		case "":
			setTipoVehiculo(Constantes.COD_TIPO_);
			break;
		case "00":
			setTipoVehiculo(Constantes.COD_TIPO_00);
			break;
		case "01":
			setTipoVehiculo(Constantes.COD_TIPO_01);
			break;
		case "02":
			setTipoVehiculo(Constantes.COD_TIPO_02);
			break;
		case "03":
			setTipoVehiculo(Constantes.COD_TIPO_03);
			break;
		case "04":
			setTipoVehiculo(Constantes.COD_TIPO_04);
			break;
		case "05":
			setTipoVehiculo(Constantes.COD_TIPO_05);
			break;
		case "06":
			setTipoVehiculo(Constantes.COD_TIPO_06);
			break;
		case "07":
			setTipoVehiculo(Constantes.COD_TIPO_07);
			break;
		case "08":
			setTipoVehiculo(Constantes.COD_TIPO_08);
			break;
		case "09":
			setTipoVehiculo(Constantes.COD_TIPO_09);
			break;
		case "0A":
			setTipoVehiculo(Constantes.COD_TIPO_0A);
			break;
		case "0B":
			setTipoVehiculo(Constantes.COD_TIPO_0B);
			break;
		case "0C":
			setTipoVehiculo(Constantes.COD_TIPO_0C);
			break;
		case "0D":
			setTipoVehiculo(Constantes.COD_TIPO_0D);
			break;
		case "0E":
			setTipoVehiculo(Constantes.COD_TIPO_0E);
			break;
		case "0F":
			setTipoVehiculo(Constantes.COD_TIPO_0F);
			break;
		case "0G":
			setTipoVehiculo(Constantes.COD_TIPO_0G);
			break;
		case "10":
			setTipoVehiculo(Constantes.COD_TIPO_10);
			break;
		case "11":
			setTipoVehiculo(Constantes.COD_TIPO_11);
			break;
		case "12":
			setTipoVehiculo(Constantes.COD_TIPO_12);
			break;
		case "13":
			setTipoVehiculo(Constantes.COD_TIPO_13);
			break;
		case "14":
			setTipoVehiculo(Constantes.COD_TIPO_14);
			break;
		case "15":
			setTipoVehiculo(Constantes.COD_TIPO_15);
			break;
		case "16":
			setTipoVehiculo(Constantes.COD_TIPO_16);
			break;
		case "17":
			setTipoVehiculo(Constantes.COD_TIPO_17);
			break;
		case "18":
			setTipoVehiculo(Constantes.COD_TIPO_18);
			break;
		case "19":
			setTipoVehiculo(Constantes.COD_TIPO_19);
			break;
		case "1A":
			setTipoVehiculo(Constantes.COD_TIPO_1A);
			break;
		case "1C":
			setTipoVehiculo(Constantes.COD_TIPO_1C);
			break;
		case "1D":
			setTipoVehiculo(Constantes.COD_TIPO_1D);
			break;
		case "1E":
			setTipoVehiculo(Constantes.COD_TIPO_1E);
			break;
		case "1F":
			setTipoVehiculo(Constantes.COD_TIPO_1F);
			break;
		case "20":
			setTipoVehiculo(Constantes.COD_TIPO_20);
			break;
		case "21":
			setTipoVehiculo(Constantes.COD_TIPO_21);
			break;
		case "22":
			setTipoVehiculo(Constantes.COD_TIPO_22);
			break;
		case "23":
			setTipoVehiculo(Constantes.COD_TIPO_23);
			break;
		case "24":
			setTipoVehiculo(Constantes.COD_TIPO_24);
			break;
		case "25":
			setTipoVehiculo(Constantes.COD_TIPO_25);
			break;
		case "30":
			setTipoVehiculo(Constantes.COD_TIPO_30);
			break;
		case "31":
			setTipoVehiculo(Constantes.COD_TIPO_31);
			break;
		case "32":
			setTipoVehiculo(Constantes.COD_TIPO_32);
			break;
		case "33":
			setTipoVehiculo(Constantes.COD_TIPO_33);
			break;
		case "34":
			setTipoVehiculo(Constantes.COD_TIPO_34);
			break;
		case "35":
			setTipoVehiculo(Constantes.COD_TIPO_35);
			break;
		case "36":
			setTipoVehiculo(Constantes.COD_TIPO_36);
			break;
		case "40":
			setTipoVehiculo(Constantes.COD_TIPO_40);
			break;
		case "50":
			setTipoVehiculo(Constantes.COD_TIPO_50);
			break;
		case "51":
			setTipoVehiculo(Constantes.COD_TIPO_51);
			break;
		case "52":
			setTipoVehiculo(Constantes.COD_TIPO_52);
			break;
		case "53":
			setTipoVehiculo(Constantes.COD_TIPO_53);
			break;
		case "54":
			setTipoVehiculo(Constantes.COD_TIPO_54);
			break;
		case "60":
			setTipoVehiculo(Constantes.COD_TIPO_60);
			break;
		case "70":
			setTipoVehiculo(Constantes.COD_TIPO_70);
			break;
		case "71":
			setTipoVehiculo(Constantes.COD_TIPO_71);
			break;
		case "72":
			setTipoVehiculo(Constantes.COD_TIPO_72);
			break;
		case "73":
			setTipoVehiculo(Constantes.COD_TIPO_73);
			break;
		case "74":
			setTipoVehiculo(Constantes.COD_TIPO_74);
			break;
		case "75":
			setTipoVehiculo(Constantes.COD_TIPO_75);
			break;
		case "76":
			setTipoVehiculo(Constantes.COD_TIPO_76);
			break;
		case "77":
			setTipoVehiculo(Constantes.COD_TIPO_77);
			break;
		case "78":
			setTipoVehiculo(Constantes.COD_TIPO_78);
			break;
		case "79":
			setTipoVehiculo(Constantes.COD_TIPO_79);
			break;
		case "7A":
			setTipoVehiculo(Constantes.COD_TIPO_7A);
			break;
		case "7B":
			setTipoVehiculo(Constantes.COD_TIPO_7B);
			break;
		case "7C":
			setTipoVehiculo(Constantes.COD_TIPO_7C);
			break;
		case "7D":
			setTipoVehiculo(Constantes.COD_TIPO_7D);
			break;
		case "7E":
			setTipoVehiculo(Constantes.COD_TIPO_7E);
			break;
		case "7F":
			setTipoVehiculo(Constantes.COD_TIPO_7F);
			break;
		case "7G":
			setTipoVehiculo(Constantes.COD_TIPO_7G);
			break;
		case "7H":
			setTipoVehiculo(Constantes.COD_TIPO_7H);
			break;
		case "7I":
			setTipoVehiculo(Constantes.COD_TIPO_7I);
			break;
		case "7J":
			setTipoVehiculo(Constantes.COD_TIPO_7J);
			break;
		case "7K":
			setTipoVehiculo(Constantes.COD_TIPO_7K);
			break;
		case "80":
			setTipoVehiculo(Constantes.COD_TIPO_80);
			break;
		case "81":
			setTipoVehiculo(Constantes.COD_TIPO_81);
			break;
		case "82":
			setTipoVehiculo(Constantes.COD_TIPO_82);
			break;
		case "90":
			setTipoVehiculo(Constantes.COD_TIPO_90);
			break;
		case "91":
			setTipoVehiculo(Constantes.COD_TIPO_91);
			break;
		case "92":
			setTipoVehiculo(Constantes.COD_TIPO_92);
			break;
		case "EX":
			setTipoVehiculo(Constantes.COD_TIPO_EX);
			break;
		case "R0":
			setTipoVehiculo(Constantes.COD_TIPO_R0);
			break;
		case "R1":
			setTipoVehiculo(Constantes.COD_TIPO_R1);
			break;
		case "R2":
			setTipoVehiculo(Constantes.COD_TIPO_R2);
			break;
		case "R3":
			setTipoVehiculo(Constantes.COD_TIPO_R3);
			break;
		case "R4":
			setTipoVehiculo(Constantes.COD_TIPO_R4);
			break;
		case "R5":
			setTipoVehiculo(Constantes.COD_TIPO_R5);
			break;
		case "R6":
			setTipoVehiculo(Constantes.COD_TIPO_R6);
			break;
		case "R7":
			setTipoVehiculo(Constantes.COD_TIPO_R7);
			break;
		case "R8":
			setTipoVehiculo(Constantes.COD_TIPO_R8);
			break;
		case "R9":
			setTipoVehiculo(Constantes.COD_TIPO_R9);
			break;
		case "RA":
			setTipoVehiculo(Constantes.COD_TIPO_RA);
			break;
		case "RB":
			setTipoVehiculo(Constantes.COD_TIPO_RB);
			break;
		case "RC":
			setTipoVehiculo(Constantes.COD_TIPO_RC);
			break;
		case "RD":
			setTipoVehiculo(Constantes.COD_TIPO_RD);
			break;
		case "RE":
			setTipoVehiculo(Constantes.COD_TIPO_RE);
			break;
		case "RF":
			setTipoVehiculo(Constantes.COD_TIPO_RF);
			break;
		case "RH":
			setTipoVehiculo(Constantes.COD_TIPO_RH);
			break;
		case "S0":
			setTipoVehiculo(Constantes.COD_TIPO_S0);
			break;
		case "S1":
			setTipoVehiculo(Constantes.COD_TIPO_S1);
			break;
		case "S2":
			setTipoVehiculo(Constantes.COD_TIPO_S2);
			break;
		case "S3":
			setTipoVehiculo(Constantes.COD_TIPO_S3);
			break;
		case "S4":
			setTipoVehiculo(Constantes.COD_TIPO_S4);
			break;
		case "S5":
			setTipoVehiculo(Constantes.COD_TIPO_S5);
			break;
		case "S6":
			setTipoVehiculo(Constantes.COD_TIPO_S6);
			break;
		case "S7":
			setTipoVehiculo(Constantes.COD_TIPO_S7);
			break;
		case "S8":
			setTipoVehiculo(Constantes.COD_TIPO_S8);
			break;
		case "S9":
			setTipoVehiculo(Constantes.COD_TIPO_S9);
			break;
		case "SA":
			setTipoVehiculo(Constantes.COD_TIPO_SA);
			break;
		case "SB":
			setTipoVehiculo(Constantes.COD_TIPO_SB);
			break;
		case "SC":
			setTipoVehiculo(Constantes.COD_TIPO_SC);
			break;
		case "SD":
			setTipoVehiculo(Constantes.COD_TIPO_SD);
			break;
		case "SE":
			setTipoVehiculo(Constantes.COD_TIPO_SE);
			break;
		case "SF":
			setTipoVehiculo(Constantes.COD_TIPO_SF);
			break;
		case "SH":
			setTipoVehiculo(Constantes.COD_TIPO_SH);
			break;
		}
		// varianteVehiculo
		setVarianteVehiculo(o_varianteVehiculo);
		// versionVehiculo
		setVersionVehiculo(o_versionVehiculo);
		// fabricante
		setFabricante(o_fabricante);
		// masaEnMarcha
		setMasaEnMarcha(o_masaEnMarcha);
		// masaMaximaTecnicaAdmisible
		setMasaMaximaTecnicaAdmisible(o_masaMaximaTecnicaAdmisible);
		// categoriaHomologacionUE
		setCategoriaHomologacionUE(o_categoriaHomologacionUE);
		// carroceria
		setCarroceria(o_carroceria);
		// plazasPie
		if (o_plazasPie.isEmpty() || !isNumeric(o_plazasPie))
			setPlazasPie(Constantes.VALOR_INDEFINIDO);
		else
			setPlazasPie(Integer.parseInt(o_plazasPie));
		// nivelEmisionesEuro
		setNivelEmisionesEuro(o_nivelEmisionesEuro);
		// consumoWHKM
		setConsumoWHKM(o_consumoWHKM);
		// clasificacionReglamento
		setClasificacionReglamento(o_clasificacionReglamento);
		// categoriaVehiculoElectrico
		setCategoriaVehiculoElectrico(o_categoriaVehiculoElectrico);
		// autonomiaVehiculoElectrico
		setAutonomiaVehiculoElectrico(o_autonomiaVehiculoElectrico);
		// marcaVehiculoBase
		setMarcaVehiculoBase(o_marcaVehiculoBase);
		// fabricanteVehiculoBase
		setFabricanteVehiculoBase(o_fabricanteVehiculoBase);
		// tipoVehiculoBase
		setTipoVehiculoBase(o_tipoVehiculoBase);
		// varianteVehiculoBase
		setVarianteVehiculoBase(o_varianteVehiculoBase);
		// versionVehiculoBase
		setVersionVehiculoBase(o_versionVehiculoBase);
		// distanciaEjes1y2 (Deber�a ser String seg�n PDF)
		if (o_distanciaEjes1y2.isEmpty() || !isNumeric(o_distanciaEjes1y2))
			setDistanciaEjes1y2(Constantes.VALOR_INDEFINIDO);
		else
			setDistanciaEjes1y2(Double.parseDouble(o_distanciaEjes1y2));
		// viaAnterior
		if (o_viaAnterior.isEmpty() || !isNumeric(o_viaAnterior))
			setViaAnterior(Constantes.VALOR_INDEFINIDO);
		else
			setViaAnterior(Double.parseDouble(o_viaAnterior));
		// viaPosterior
		if (o_viaPosterior.isEmpty() || !isNumeric(o_viaPosterior))
			setViaPosterior(Constantes.VALOR_INDEFINIDO);
		else
			setViaPosterior(Double.parseDouble(o_viaPosterior));
		// tipoAlimentacion
		if (o_tipoAlimentacion.equalsIgnoreCase("m"))
			setTipoAlimentacion(Constantes.TIPO_ALIMENTACION_MONOCOMBUSTIBLE);
		else if (o_tipoAlimentacion.equalsIgnoreCase("b"))
			setTipoAlimentacion(Constantes.TIPO_ALIMENTACION_BICOMBUSTIBLE);
		else if (o_tipoAlimentacion.equalsIgnoreCase("f"))
			setTipoAlimentacion(Constantes.TIPO_ALIMENTACION_FLEXICOMBUSTIBLE);
		// contrasenaHomologacion
		setContrasenaHomologacion(o_contrasenaHomologacion);
		// ecoInnovacion
		if (o_ecoInnovacion.isEmpty())
			setEcoInnovacion(Constantes.SIN_DEFINIR_UE);
		else if (o_ecoInnovacion.equalsIgnoreCase("s"))
			setEcoInnovacion("SI");
		else if (o_ecoInnovacion.equalsIgnoreCase("n"))
			setEcoInnovacion("NO");
		// reduccionEco (Sin definir a�n por la UE)
		setReduccionEco(Constantes.SIN_DEFINIR_UE);
		// codigoEco (Sin definir a�n por la UE)
		setCodigoEco(Constantes.SIN_DEFINIR_UE);
		// fechaProceso
		if (!o_fechaProceso.isEmpty()) {
			try {
				setFechaProceso(new SimpleDateFormat("ddMMyyyy").parse(o_fechaProceso));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("[ERROR] No se ha podido calcular el valor fechaProceso.");
				e.printStackTrace();
			}
		} else
			setFechaProceso(null);
	}
	
}
