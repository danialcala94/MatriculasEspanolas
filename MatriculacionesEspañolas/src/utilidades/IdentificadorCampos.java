package utilidades;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import entradasalida.Importar;
import objetos.Vehiculo;

public class IdentificadorCampos {
	
	private static int longitudCampos[] = {8, 1, 8, 30, 22, 1, 21, 2, 1, 5, 6, 6, 6, 3, 2, 2, 2, 2, 24, 2, 2, 
			1, 8, 5, 8, 1, 1, 9, 3, 5, 30, 7, 3, 5, 1, 1, 1, 1, 1, 1, 11, 25, 25, 35, 70, 6, 6, 4, 4, 3, 8, 4,
			4, 4, 6, 30, 50, 35, 25, 35, 4, 4, 4, 1, 25, 1, 4, 25, 8};

	public static ArrayList<String> obtenerCamposPorLongitud(String fichero) {
		ArrayList<String> datos = new ArrayList<String>();
		
		String textoLeido = "";
		try {
			textoLeido += Importar.leerContenido(fichero);
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: No se ha encontrado el fichero.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ERROR: No se ha podido leer el fichero.");
			e.printStackTrace();
		}
		
		// Eliminamos 79 caracteres correspondientes a la cabecera
		//textoLeido = textoLeido.substring(79, textoLeido.length());
		
		int indice = 0;
		int i = 0;
		String textoAuxiliar = textoLeido;
		int tamanoTotal = textoAuxiliar.length();
		while (!textoAuxiliar.isEmpty()) {
			//System.out.println("-___---___--" + textoLeido.length() + " ----_--- " + textoAuxiliar.length());
			//System.out.println(textoLeido.substring(indice, indice + longitudCampos[i]).trim());
			datos.add(textoLeido.substring(indice, indice + longitudCampos[i]).trim());
			
			textoAuxiliar = textoAuxiliar.substring(longitudCampos[i], textoAuxiliar.length());
			
			indice += longitudCampos[i];
			
			i++;
			if (i >= longitudCampos.length) {
				i = 0;
				System.out.println(100 - (float)textoAuxiliar.length() / tamanoTotal * 100 + "%");
			}
		}
		//System.out.println("------------------------------------------------------------------");
		//System.out.println("------------------------------------------------------------------");
		//System.out.println("------------------------------------------------------------------");
		//System.out.println("------------------------------------------------------------------");
		
		return datos;
	}
	
	public static ArrayList<String> obtenerCampos(String fichero) {
		ArrayList<String> datos = new ArrayList<String>();
		
		String textoLeido = "";
		try {
			textoLeido += Importar.leerContenido(fichero);
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: No se ha encontrado el fichero.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ERROR: No se ha podido leer el fichero.");
			e.printStackTrace();
		}
		
		String[] textoExtraido = textoLeido.trim().split(" ");
		for (int i = 0; i < textoExtraido.length; i++) {
			boolean limpiar = true;
			for (int j = 0; j < textoExtraido[i].length(); j++) {
				if (textoExtraido[i].charAt(j) != ' ') {
					limpiar = false;
					break;
				}
			}
			if (limpiar)
				textoExtraido[i] = "";
			if (!textoExtraido[i].isEmpty())
				datos.add(textoExtraido[i].toString());
		}
		
		return datos;
	}
	
	public static ArrayList<Vehiculo> obtenerVehiculos(String fichero) {
		ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
		
		ArrayList<String> caracteristicas = obtenerCamposPorLongitud(fichero);
		Vehiculo veh = new Vehiculo();
		for (int i = 0; i < caracteristicas.size(); i++) {
			String caracteristica = caracteristicas.get(i);
			switch (i % Constantes.NUMERO_CAMPOS_VEHICULO) {
			case 0:
				veh.setO_fechaMatriculacion(caracteristica);
				break;
			case 1:
				veh.setO_codigoClaseMatricula(caracteristica);
				break;
			case 2:
				veh.setO_fechaTramitacion(caracteristica);
				break;
			case 3:
				veh.setO_marcaITV(caracteristica);
				break;
			case 4:
				veh.setO_modeloITV(caracteristica);
				break;
			case 5:
				veh.setO_codigoProcedencia(caracteristica);
				break;
			case 6:
				veh.setO_numeroBastidor(caracteristica);
				break;
			case 7:
				veh.setO_codigoTipoVehiculo(caracteristica);
				break;
			case 8:
				veh.setO_codigoTipoPropulsion(caracteristica);
				break;
			case 9:
				veh.setO_cilindrada(caracteristica);
				break;
			case 10:
				veh.setO_potencia(caracteristica);
				break;
			case 11:
				veh.setO_tara(caracteristica);
				break;
			case 12:
				veh.setO_pesoMaximo(caracteristica);
				break;
			case 13:
				veh.setO_numeroPlazas(caracteristica);
				break;
			case 14:
				veh.setO_esPrecintado(caracteristica);
				break;
			case 15:
				veh.setO_esEmbargado(caracteristica);
				break;
			case 16:
				veh.setO_numeroTransmisiones(caracteristica);
				break;
			case 17:
				veh.setO_numeroTitulares(caracteristica);
				break;
			case 18:
				veh.setO_localidadDomicilio(caracteristica);
				break;
			case 19:
				veh.setO_codigoProvinciaDomicilio(caracteristica);
				break;
			case 20:
				veh.setO_codigoProvinciaMatricula(caracteristica);
				break;
			case 21:
				veh.setO_claveTramite(caracteristica);
				break;
			case 22:
				veh.setO_fechaTramite(caracteristica);
				break;
			case 23:
				veh.setO_codigoPostalDomicilio(caracteristica);
				break;
			case 24:
				veh.setO_fechaPrimeraMatriculacion(caracteristica);
				break;
			case 25:
				veh.setO_esNuevoOUsado(caracteristica);
				break;
			case 26:
				veh.setO_titularPersonaJuridicaOFisica(caracteristica);
				break;
			case 27:
				veh.setO_codigoITV(caracteristica);
				break;
			case 28:
				veh.setO_servicio(caracteristica);
				break;
			case 29:
				veh.setO_codigoINEMunicipioDomicilio(caracteristica);
				break;
			case 30:
				veh.setO_municipio(caracteristica);
				break;
			case 31:
				veh.setO_potenciaNetaMaximaKW(caracteristica);
				break;
			case 32:
				veh.setO_numeroMaximoPlazas(caracteristica);
				break;
			case 33:
				veh.setO_emisionesCO2(caracteristica);
				break;
			case 34:
				veh.setO_esRenting(caracteristica);
				break;
			case 35:
				veh.setO_codigoTutela(caracteristica);
				break;
			case 36:
				veh.setO_codigoPosesion(caracteristica);
				break;
			case 37:
				veh.setO_codigoBajaDefinitiva(caracteristica);
				break;
			case 38:
				veh.setO_esBajaTemporal(caracteristica);
				break;
			case 39:
				veh.setO_esRobado(caracteristica);
				break;
			case 40:
				veh.setO_esBajaTelematica(caracteristica);
				break;
			case 41:
				veh.setO_tipoVehiculo(caracteristica);
				break;
			case 42:
				veh.setO_varianteVehiculo(caracteristica);
				break;
			case 43:
				veh.setO_versionVehiculo(caracteristica);
				break;
			case 44:
				veh.setO_fabricante(caracteristica);
				break;
			case 45:
				veh.setO_masaEnMarcha(caracteristica);
				break;
			case 46:
				veh.setO_masaMaximaTecnicaAdmisible(caracteristica);
				break;
			case 47:
				veh.setO_categoriaHomologacionUE(caracteristica);
				break;
			case 48:
				veh.setO_carroceria(caracteristica);
				break;
			case 49:
				veh.setO_plazasPie(caracteristica);
				break;
			case 50:
				veh.setO_nivelEmisionesEuro(caracteristica);
				break;
			case 51:
				veh.setO_consumoWHKM(caracteristica);
				break;
			case 52:
				veh.setO_clasificacionReglamento(caracteristica);
				break;
			case 53:
				veh.setO_categoriaVehiculoElectrico(caracteristica);
				break;
			case 54:
				veh.setO_autonomiaVehiculoElectrico(caracteristica);
				break;
			case 55:
				veh.setO_marcaVehiculoBase(caracteristica);
				break;
			case 56:
				veh.setO_fabricanteVehiculoBase(caracteristica);
				break;
			case 57:
				veh.setO_tipoVehiculoBase(caracteristica);
				break;
			case 58:
				veh.setO_varianteVehiculoBase(caracteristica);
				break;
			case 59:
				veh.setO_versionVehiculoBase(caracteristica);
				break;
			case 60:
				veh.setO_distanciaEjes1y2(caracteristica);
				break;
			case 61:
				veh.setO_viaAnterior(caracteristica);
				break;
			case 62:
				veh.setO_viaPosterior(caracteristica);
				break;
			case 63:
				veh.setO_tipoAlimentacion(caracteristica);
				break;
			case 64:
				veh.setO_contrasenaHomologacion(caracteristica);
				break;
			case 65:
				veh.setO_ecoInnovacion(caracteristica);
				break;
			case 66:
				veh.setO_reduccionEco(caracteristica);
				break;
			case 67:
				veh.setO_codigoEco(caracteristica);
				break;
			case 68:
				veh.setO_fechaProceso(caracteristica);
				break;
			}
			
			veh.anadirAtributo(caracteristica);
			
			if ((i + 1) % Constantes.NUMERO_CAMPOS_VEHICULO == 0) {
				vehiculos.add(veh);
				veh = new Vehiculo();
			}
		}
		
		return vehiculos;
	}
	
}
