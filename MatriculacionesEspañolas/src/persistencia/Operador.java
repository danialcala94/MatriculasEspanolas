package persistencia;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import objetos.Vehiculo;

public class Operador {

	final static String DB_CONNECTION_SENTENCE = "jdbc:mysql://localhost:3306/" + Operador.DB_NAME;
	final static String DB_USERNAME = "root";
	final static String DB_PASSWORD = "";
	final static String DB_NAME = "matriculacionesespañolas";
	
	final static String INSERT_INTO_MATRICULACIONES = "INSERT INTO matriculaciones (fechaMatriculacion, codigoClaseMatricula, fechaTramitacion, marca, modelo, "
			+ "codigoProcedencia, numeroBastidor, codigoTipoVehiculo, codigoTipoPropulsion, cilindrada, potencia, tara, "
			+ "pesoMaximo, numeroPlazas, esPrecintado, esEmbargado, numeroTransmisiones, numeroTitulares, localidadDomicilio, "
			+ "provinciaDomicilio, provinciaMatriculacion, claveTramite, fechaTramite, codigoPostalDomicilio, fechaPrimeraMatriculacion, "
			+ "esNuevoOUsado, esTitularPersonaFisicaOJuridica, codigoITV, codigoServicio, codigoINEMunicipioDomicilio, municipio, potenciaNetaMaximaKW, "
			+ "numeroMaximoPlazas, emisionesCO2, esRenting, codigoTutela, codigoPosesion, codigoBajaDefinitiva, esBajaTemporal, esRobado, esBajaTelematica, "
			+ "tipoVehiculo, varianteVehiculo, versionVehiculo, fabricante, masaEnMarcha, masaMaximaTecnicaAdmisible, categoriaHomologacionUE, carroceria, "
			+ "plazasPie, nivelEmisionesEuro, consumoWHKM, clasificacionReglamento, categoriaVehiculoElectrico, autonomiaVehiculoElectrico, marcaVehiculoBase, "
			+ "fabricanteVehiculoBase, tipoVehiculoBase, varianteVehiculoBase, versionVehiculoBase, distanciaEjes1y2, viaAnterior, viaPosterior, tipoAlimentacion, "
			+ "contrasenaHomologacion, ecoInnovacion, reduccionEco, codigoEco, fechaProceso) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
			+ " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	
private static Operador instanciaFachadaPersistencia;
	
	private static Connection cn = null;
	
	public static synchronized Operador getFachadaPersistencia() {
		if(instanciaFachadaPersistencia == null) {
			instanciaFachadaPersistencia = new Operador();
		}
		return instanciaFachadaPersistencia;
	}
	
	public static synchronized Connection getConnection() {
		if (cn == null) {
			conectar();
		}
		return cn;
	}
	
	public static synchronized void conectar() {
		if (cn == null) {
			try {
				System.out.println("Conectando con: " + DB_CONNECTION_SENTENCE);
				cn = DriverManager.getConnection(DB_CONNECTION_SENTENCE, DB_USERNAME, DB_PASSWORD);
			} catch (SQLException sqlExc) {
				System.err.println("SQLException - Error al conectar: " + sqlExc.getMessage());
			}
		}
	}
	
	public static synchronized void desconectar() {
		if (cn != null) {
			try {
				System.out.println("Desconectándose de: " + DB_CONNECTION_SENTENCE);
				cn.close();
			} catch (SQLException sqlExc) {
				System.err.println("SQLException - Error al desconectar: " + sqlExc.getMessage());
			}
		}
		cn = null;
	}
	
	public synchronized Object almacenarVehiculo(Vehiculo objeto) {
		// TODO Auto-generated method stub
		Vehiculo vehiculo = objeto;
		
		// Comprobación de seguridad
		boolean esCorrecto = true;
		if (vehiculo.getO_fechaPrimeraMatriculacion() == null)
			esCorrecto = false;
		// ¿Comprobación de repetidos?
		
		// Guardar el nuevo plato
		if (esCorrecto) {
			try {
				// Gestionar con autocommit y rollback y commit
				Operador.getConnection().setAutoCommit(false);
				PreparedStatement ps = Operador.getConnection().prepareStatement(Operador.INSERT_INTO_MATRICULACIONES);
				
				ps.setDate(1, new Date(vehiculo.getFechaMatriculacion().getTime()));
				ps.setString(2, vehiculo.getCodigoClaseMatricula());
				if (vehiculo.getFechaTramitacion() != null)
					ps.setDate(3, new Date(vehiculo.getFechaTramitacion().getTime()));
				else
					ps.setDate(3, (Date) vehiculo.getFechaTramitacion());
				ps.setString(4, vehiculo.getMarca());
				ps.setString(5, vehiculo.getModelo());
				ps.setString(6, vehiculo.getCodigoProcedencia());
				ps.setString(7, vehiculo.getNumeroBastidor());
				ps.setString(8, vehiculo.getCodigoTipoVehiculo());
				ps.setString(9, vehiculo.getCodigoTipoPropulsion());
				ps.setDouble(10, vehiculo.getCilindrada());
				ps.setDouble(11, vehiculo.getPotencia());
				ps.setDouble(12, vehiculo.getTara());
				ps.setDouble(13, vehiculo.getPesoMaximo());
				ps.setInt(14, vehiculo.getNumeroPlazas());
				ps.setBoolean(15, vehiculo.isEsPrecintado());
				ps.setBoolean(16, vehiculo.isEsEmbargado());
				ps.setInt(17, vehiculo.getNumeroTransmisiones());
				ps.setInt(18, vehiculo.getNumeroTitulares());
				ps.setString(19, vehiculo.getLocalidadDomicilio());
				ps.setString(20, vehiculo.getProvinciaDomicilio());
				ps.setString(21, vehiculo.getProvinciaMatriculacion());
				ps.setString(22, vehiculo.getClaveTramite());
				if (vehiculo.getFechaTramitacion() != null)
					ps.setDate(23, new Date(vehiculo.getFechaTramitacion().getTime()));
				else
					ps.setDate(23, (Date) vehiculo.getFechaTramitacion());
				ps.setInt(24, vehiculo.getCodigoPostalDomicilio());
				if (vehiculo.getFechaPrimeraMatriculacion() != null)
					ps.setDate(25, new Date(vehiculo.getFechaPrimeraMatriculacion().getTime()));
				else
					ps.setDate(25, (Date) vehiculo.getFechaPrimeraMatriculacion());
				ps.setString(26, vehiculo.getEsNuevoOUsado());
				ps.setString(27, vehiculo.getEsTitularPersonaFisicaOJuridica());
				ps.setString(28, vehiculo.getCodigoITV());
				ps.setString(29, vehiculo.getCodigoServicio());
				ps.setInt(30, vehiculo.getCodigoINEMunicipioDomicilio());
				ps.setString(31, vehiculo.getMunicipio());
				ps.setDouble(32, vehiculo.getPotenciaNetaMaximaKW());
				ps.setInt(33, vehiculo.getNumeroMaximoPlazas());
				ps.setInt(34, vehiculo.getEmisionesCO2());
				ps.setBoolean(35, vehiculo.isEsRenting());
				ps.setString(36, vehiculo.getCodigoTutela());
				ps.setString(37, vehiculo.getCodigoPosesion());
				ps.setString(38, vehiculo.getCodigoBajaDefinitiva());
				ps.setBoolean(39, vehiculo.isEsBajaTemporal());
				ps.setBoolean(40, vehiculo.isEsRobado());
				ps.setBoolean(41, vehiculo.isEsBajaTelematica());
				ps.setString(42, vehiculo.getTipoVehiculo());
				ps.setString(43, vehiculo.getVarianteVehiculo());
				ps.setString(44, vehiculo.getVersionVehiculo());
				ps.setString(45, vehiculo.getFabricante());
				ps.setString(46, vehiculo.getMasaEnMarcha());
				ps.setString(47, vehiculo.getMasaMaximaTecnicaAdmisible());
				ps.setString(48, vehiculo.getCategoriaHomologacionUE());
				ps.setString(49, vehiculo.getCarroceria());
				ps.setInt(50, vehiculo.getPlazasPie());
				ps.setString(51, vehiculo.getNivelEmisionesEuro());
				ps.setString(52, vehiculo.getConsumoWHKM());
				ps.setString(53, vehiculo.getClasificacionReglamento());
				ps.setString(54, vehiculo.getCategoriaVehiculoElectrico());
				ps.setString(55, vehiculo.getAutonomiaVehiculoElectrico());
				ps.setString(56, vehiculo.getMarcaVehiculoBase());
				ps.setString(57, vehiculo.getFabricanteVehiculoBase());
				ps.setString(58, vehiculo.getTipoVehiculoBase());
				ps.setString(59, vehiculo.getVarianteVehiculoBase());
				ps.setString(60, vehiculo.getVersionVehiculoBase());
				ps.setDouble(61, vehiculo.getDistanciaEjes1y2());
				ps.setDouble(62, vehiculo.getViaAnterior());
				ps.setDouble(63, vehiculo.getViaPosterior());
				ps.setString(64, vehiculo.getTipoAlimentacion());
				ps.setString(65, vehiculo.getContrasenaHomologacion());
				ps.setString(66, vehiculo.getEcoInnovacion());
				ps.setString(67, vehiculo.getReduccionEco());
				ps.setString(68, vehiculo.getCodigoEco());
				if (vehiculo.getFechaProceso() != null)
					ps.setDate(69, new Date(vehiculo.getFechaProceso().getTime()));
				else
					ps.setDate(69, (Date) vehiculo.getFechaProceso());
				
				ps.executeUpdate();
				Operador.getConnection().commit();
				Operador.getConnection().setAutoCommit(true);
				System.out.println("Se ha creado la matriculación " + vehiculo.getMarca());
				//cacheActualizada = false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				try {
					Operador.getConnection().rollback();
					System.err.println("Haciendo rollback...");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Error en el rollback: ");
					e1.printStackTrace();
					return null;
				}
				System.err.println("No se ha podido añadir el alérgeno " + vehiculo.getFechaPrimeraMatriculacion());
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Compruebe que no ocurra esto: vehiculo.getFechaPrimeraMatriculacion == null");
			return null;
		}
		return vehiculo;
	}
	
}
